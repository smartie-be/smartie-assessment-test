# Introduction

As we want to get to know you as a person, start by introducing yourself to the audience.
Take the following questions into mind when you build your introduction.

- What is your background and education ?
- Why are you interested in a position as data scientist or AI specialist ?
- If you become part of the team, what do you believe that you can add to the team ?
- What areas of DS and AI are you most interested in ?
- Which companies and sectors would you love to work for as a consultant of Smartie ?


# General questions

- Explain the difference between supervised and unsupervised learning and give an example.
- What do the concepts "whitebox" and "blackbox" methods mean and give an example.
- When modelling a linear regression, does a higher R2 (R squared) always mean that you have a better model ? Why (not) ? If remedying the situation is necessary, how do we do this ?