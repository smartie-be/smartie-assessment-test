# Smartie Assessment

This repository contains tests for all the Data-Enthusiasts and believers who wants to join and be part of the Smartie team.

As a team, we need to know to get to know you and how you think/work, in order to provide you the best support so you have the best chance to shine and excel.
That's why we have this assessment.

If you are a data scientist or aspire to be one, you should be proud of your skills and abilities.
There is no better way of convincing us then just showing us.

This assessment offers 3 parts :

- General
- Deep Learning
- NLP

## Guidelines of the assessment

- You have exactly 14 days to complete the assessment. We send you the link to the test via e-mail. **We expect a confirmation e-mail from you. The timestamp of your e-mail is when your clock starts to run.**
- You only have to **complete 2 parts**.
- **General is mandatory**, but you can choose Deep Learning or NLP, based on your preference.
- How you solve the problems is completely up to you, so make use of your weapon of choice.
- We expect the complete dataset and solution by the end of the 14 days.
- You can expect feedback within 10 days after submission of the assignment.
- **If the submission looks promising, you will be invited to present your solution to our whole team. It's a good way for you to get to know us all and determine if you would like to be part of our team.**

If you have any problems, we are always open to provide advice.
After all, we might become colleagues soon.

**Good luck with the assessment, but most importantly enjoy the challenge and experience.**

![](http://cdn2-www.comingsoon.net/assets/uploads/gallery/the-x-files-ew-photos/key-art-4-hand.jpg)
